from django.shortcuts import render,redirect, get_object_or_404
from .models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from .forms import AddReceiptform, AddExpenseCategoryform, AddAccountform


@login_required
def home(request):
    receipt = Receipt.objects.filter(purchaser = request.user)
    context = {
        "receipt_list": receipt,
    }
    return render(request, "receipts_temp/home.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
      form = AddReceiptform(request.POST)
      if form.is_valid():
          receipt = form.save(False)
          receipt.purchaser = request.user
          receipt.save()
          return redirect("home")
    else:
        form = AddReceiptform()
        context = {
            "form": form,
        }
    return render(request, 'receipts_temp/create_receipts.html', context)

@login_required
def category_list(request):
    categoryitems = ExpenseCategory.objects.filter(owner = request.user)
    context = {
        "category_items": categoryitems,
    }
    return render(request, "receipts_temp/receipt_categories.html", context)

@login_required
def account_list(request):
    accountitems = Account.objects.filter(owner = request.user)
    context = {
        "account_items": accountitems,
    }
    return render(request, "receipts_temp/accounts.html", context)

@login_required
def create_category(request):
    if request.method == "POST":
      form = AddExpenseCategoryform(request.POST)
      if form.is_valid():
          receipt = form.save(False)
          receipt.owner = request.user
          receipt.save()
          return redirect("category_list")
    else:
        form = AddExpenseCategoryform()
        context = {
            "form": form,
        }
    return render(request, "receipts_temp/new_ExpenseCategory.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
      form = AddAccountform(request.POST)
      if form.is_valid():
          receipt = form.save(False)
          receipt.owner = request.user
          receipt.save()
          return redirect("account_list")
    else:
        form = AddAccountform()
        context = {
            "form": form,
        }
    return render(request, "receipts_temp/create_account.html", context)
