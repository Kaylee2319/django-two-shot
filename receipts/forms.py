from django.forms import ModelForm
from .models import ExpenseCategory, Account, Receipt



class AddReceiptform(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]

class AddExpenseCategoryform(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]


class AddAccountform(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]
